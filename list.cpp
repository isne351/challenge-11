﻿#include <iostream>
#include "list.h"
using namespace std;
	
void List::headPush(int var) {
	
	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		head = new Node(var,0, 0);
		tail = head;
	} else {
		tmp = new Node(var,head,0);	

		head->prev = tmp;
		head = tmp;
	}

}

void List::tailPush(int var) {

	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		tail = new Node(var,0,tail); // Don't need tmp ;)
		head = tail;
	} else {
		tmp = new Node(var,0, tail); // Don't need tmp ;)
		tail->next = tmp;
		tail = tmp;
	}

}

int List::headPop() {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}

	Node *tmp = head->next;

	delete head;

	head = tmp;

	head->prev = 0;

	return head->info;

}

int List::tailPop() {
	
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}
	
	Node *tmp;

	tmp = tail->prev;

	delete tail;

	tail = tmp;

	tmp->next = 0;

}

bool List::isInList(int search) {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}

	Node *tmp = tail;

	bool result;

	result = false;

	cout << "Serach : " << search << " IN( ";

	while (true) {
		
		cout << " " << tmp->info << " ";

		if (tmp->info == search) result = true;
		
		tmp = tmp->prev; // next pointer
	
		if (tmp == 0)break;

	}

	cout << " )";

	return result; // return true if found

}

void List::deleteNode(int search){

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return;
	}

	Node *tmp = head; // pointer

	while (true) {

		if (tmp->info == search) {
			if (tmp == tail) { // check last element
				List::tailPop(); //tailpop ;)
				return;
			} else if(tmp == head) {
				List::headPop(); //headpop ;)
				return;
			} else {
				
				Node *copy = tmp->prev;

				copy->next = tmp->next;

				tmp->next->prev = copy;

				delete tmp;  // delete tmp

				tmp = copy;  // tmp_2 -> skip

				return;
			
			}
		}

		tmp = tmp->next;

		if (tmp == 0) return;

	}
}

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}