#include <iostream>
#include "list.h"
using namespace std;

void main(){

	List a;

	cout << a.headPop() << endl;

	for (int i = 6; i > 0; i--) {
		a.headPush(i);
		cout << "HeadPush : " << i << endl;
	}

	if (a.isInList(2) == 1) {
		cout << " Found" << endl;
	}

	for (int i = 7; i < 10; i++) {
		a.tailPush(i);
		cout << "TailPush : " << i << endl;
	}

	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	}
	else {
		cout << " Found" << endl;
	}

	cout << "Delete : 5 " << endl;

	a.deleteNode(9);
	
	a.deleteNode(1);

	a.deleteNode(5);

	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	} else {
		cout << " Found" << endl;
	}

	a.tailPush(5);
	cout << "TailPush :  5 " << endl;

	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	}
	else {
		cout << " Found" << endl;
	}

	system("pause");

}